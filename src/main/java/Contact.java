package main.java;

public class Contact {

    private String name;

    public Contact(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
